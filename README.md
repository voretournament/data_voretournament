This repository contains the data directory of the Vore Tournament mod for Xonotic.

# Release Information

**Download:**

- [2.0.1 (Xonotic 0.8.2) 16-07-2019](https://gitlab.com/voretournament/data_voretournament/-/archive/2.0.1/data_voretournament-2.0.1.zip)
- [2.0.0 (Xonotic 0.8.2) 10-07-2017](https://gitlab.com/voretournament/data_voretournament/-/archive/2.0.0/data_voretournament-2.0.0.zip)

[**Changelog**](https://gitlab.com/voretournament/data_voretournament/blob/master/CHANGES.md)

[**Credits**](https://gitlab.com/voretournament/data_voretournament/blob/master/CREDITS.md)

# Instructions

Below are instructions for downloading and running Vore Tournament. Please keep in mind that being a Linux user, I can't personally test the steps on every platform: The instructions provided for Windows are based on research and guesses, whereas for Mac I have no knowledge of the compilation procedures. If you're not familiar with software development and how to compile, you may have to do some research into how the mentioned tools work. You may also want to use alternative tools, in case the ones suggested here don't work or you prefer others... the instructions below are for TortoiseGit and msys2.

If these instructions don't work for you, feel free to ask for support on IRC at: Server irc.freenode.org, channel #xonotic. Note that Vore Tournament is not developed by the Xonotic team... as such the channel can only cover questions about the engine, qmqcc, and general compilation procedures.

## Running the release version:

1. Download and install the latest version of Xonotic from http://xonotic.org You might want to try a vanilla game first, to make sure the base game is working properly.
2. Download the archive of the latest Vore Tournament release from this repository using the download button provided.
3. Unpack this archive inside the base Xonotic directory, so that the "data_voretournament-*" directory is sitting next to the "data" directory.
4. Rename "data_voretournament-*" to "data_voretournament", deleting the extra names and numbers from the directory name.
5. Launch Vore Tournament by running the "voretournament-*.cmd" (Windows) or "voretournament-*.sh" (Linux) scripts for your platform (x64 for 64-bit, x86 for 32-bit). If neither of those files work for your platform, manually create a shortcut to the Xonotic binary and add the parameters "-customgamedirname2 data_voretournament" at the end of the target call. Example: "xonotic.exe -customgamedirname2 data_voretournament".

## Running the development version:

1. Prerequisites: The most essential tool is git, which is necessary to clone Xonotic and Vore Tournament.
- Linux: Make sure your distribution's git packages are installed.
- Windows: Go to https://tortoisegit.org/ and install TortoiseGit.
2. Prerequisites: If you plan to compile the components yourself, you will also need the C++ compilers for your platform.
- Linux: Make sure your distribution's gcc packages are installed.
- Windows: Go to https://sourceforge.net/projects/msys2/ and install msys2. After that run the deps.sh script included in the Vore Tournament repository, which will install the needed dependencies.
3. GIT: Clone the Xonotic project from https://gitlab.com/xonotic/xonotic anywhere on your drive.
- Linux: Use the command: 'git clone https://gitlab.com/xonotic/xonotic.git'.
- Windows: Use the clone menu in TortoiseGit and clone the URL: 'https://gitlab.com/xonotic/xonotic.git'.
4. GIT: Download all of Xonotic's sub-repositories, which are listed under the https://gitlab.com/groups/xonotic group. To do this run './all update' from the base Xonotic directory. If this fails, manually clone each repository ending in .pk3dir inside xonotic/data, as well as at least the repositories "darkplaces" and "gmqcc" directly inside xonotic.
- Linux: See the instructions at step 3.
- Windows: See the instructions at step 3.
5. GIT: Clone the Vore Tournament project from https://gitlab.com/voretournament/data_voretournament inside the base Xonotic directory (eg: next to the 'data' directory).
- Linux: See the instructions at step 3.
- Windows: See the instructions at step 3.
6. Components: Obtain the Darkplaces engine.
- Compile, Linux: Use './all compile' in the base Xonotic directory, or the 'make release' command under xonotic/darkplaces if that fails.
- Compile, Windows: Open a msys2 shell and replicate the same step as for Linux.
- Download: If you have trouble compiling, you can alternatively download Darkplaces; Go to https://icculus.org/twilight/darkplaces/ and get the latest nightly build for your platform and architecture.
- Notes: If the binaries are called darkplaces, they need to be renamed to xonotic (eg: darkplaces.exe to xonotic.exe). The binaries must be located in the base Xonotic directory. You must also copy all of the required dll files here from xonotic/misc/buildfiles/win64 (win32 on 32bit systems).
7. Components: Obtain the gmqcc compiler. Note that this is only needed if you plan to compile the game code yourself at step 9.
- Compile, Linux: Use './all compile' in the base Xonotic directory, or the 'make all' command under xonotic/gmqcc if that fails.
- Compile, Windows: Open a msys2 shell and replicate the same step as for Linux.
- Download: If you have trouble compiling, you can alternatively download gmqcc; Go to http://graphitemaster.github.io/gmqcc/download.html and get the latest version for your platform and architecture.
- Notes: The gmqcc binaries need to be located inside the xonotic/gmqcc directory.
8. Components: Obtain the game code.
- Compile, Linux: Run the build.sh script included in the Vore Tournament repository. If that fails issue the following command from the Xonotic base directory: 'make -C ./data/xonotic-data.pk3dir/qcsrc/ BUILD_MOD=../../../data_voretournament/'.
- Compile, Windows: Open a msys2 shell and replicate the same step as for Linux.
- Download: If you have trouble compiling, you can alternatively download a snapshot of the game code; Clone the repository https://gitlab.com/voretournament/voretournament-qcsrc.pk3dir.git inside your data_voretournament directory.
- Notes: You should have 6 files in total: csprogs, progs, menu... a dat and a lno of each (eg: progs.lno). Those files must be located inside the xonotic/data/xonotic-data.pk3dir directory.
9. Launch Vore Tournament.
- Windows: Start the binary from the base Xonotic directory, calling it as follows: 'xonotic.exe -customgamedirname2 data_voretournament'.
- Linux: Start the binary from the base Xonotic directory, calling it as follows: 'xonotic-linux-glx.sh -customgamedirname2 data_voretournament'.
10. If you did everything right and the game starts, you will need a few maps to play on! Go to http://beta.xonotic.org/autobuild-bsp/ and download the fullpk3 of each map you want. Place every pk3 file inside xonotic/data and the engine should see it.
11. Remember that both Xonotic and Vore Tournament are frequently updated. Always issue a './all update' from the base directory, use the 'git pull' command in each repository within xonotic/data, or use Tortoise if you're on Windows... then update data_voretournament as well. Recompile the game code after doing so to apply all code changes.
